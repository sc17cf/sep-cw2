import os

WTF_CSRF_ENABLED = True
SECRET_KEY = '61133898cd9d6f32a795130ebcb585f4'

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_TRACK_MODIFICATIONS = True
