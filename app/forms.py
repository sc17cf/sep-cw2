from flask_wtf import Form
from wtforms import StringField, TextField, SubmitField, FieldList, FormField, BooleanField, SelectField
from wtforms.validators import DataRequired
from wtforms.widgets import TextArea
from wtforms.fields.html5 import DateField

class NewTaskForm(Form):
    taskName = StringField('Event Name', validators=[DataRequired()])
    dueDay = SelectField('Day', choices=[("Monday","Monday"),("Tuesday","Tuesday"),("Wednesday","Wednesday"),("Thursday","Thursday"),("Friday","Friday"),("Saturday","Saturday"),("Sunday","Sunday")])
    duration = StringField('Duration', validators=[DataRequired()])
    description = StringField(validators=[DataRequired()], widget=TextArea())
    submit = SubmitField('Create event')
