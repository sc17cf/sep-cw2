from app import app
from flask import render_template, redirect, url_for, flash, request
from .forms import NewTaskForm
from .models import Task
from app import db, models
from sqlalchemy import update
import datetime

def getEvent():
    event = [["Wednesday","Football","14:00","3"]]

def timeTableAPI(username):
    timetable = [["Monday","COMP2421 Tutorial","10:00","1","University"],
                ["Monday","COMP2711 Lecture","11:00","1","University"],
                ["Monday","COMP2211 Lecture","16:00","1","University"],
                ["Tuesday","Labs","11:00","4","University"],
                ["Wednesday","COMP2421 Lecture","10:00","1","University"],
                ["Wednesday","COMP2811 Lecture","11:00","1","University"],
                ["Wednesday","COMP2912 Lecture","12:00","1","University"],
                ["Wednesday","Football","14:00","4","Event"],
                ["Thursday","Labs","10:00","2","University"],
                ["Thursday","COMP2711 Lecture","12:00","1","University"],
                ["Thursday","COMP2912 Lecture","15:00","1","University"],
                ["Thursday","COMP2211 Lecture","16:00","1","University"],
                ["Thursday","COMP2421 Lecture","17:00","1","University"],
                ["Friday","COMP2711 Lecture","11:00","1","University"],
                ["Friday","COMP2811 Lecture","14:00","1","University"],
                ["Friday","Pub","19:00","2","Event"]]
    return timetable

def courseworkAPI(username):
    timetable = [["Software Engineering Coursework 2","06-12-2018","10:00","9","Create a proof of concept for your design brief"],
                ["User Interfaces Coursework 3","07-12-2018","12:00","9","Create a GUI interface from git"],
                ["Numerical Computation Coursework 2","10-12-2018","10:00","3","Complete summative coursework 8"],
                ["Web Application Development","14-12-2018","10:00","60","Create a website accessable from the internet"]]
    return timetable

def createSchedule(username):
    #In production would be done client side

    timetable = timeTableAPI(username)                            #Stored on uni server
    coursework = courseworkScheduleAPI(username)        #Would be stored on uni server
    events = getEvents()                                                    #Events stored on client device in events db

    for event in events:
        timetable.append(event) #add events to timetable



@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route('/login',  methods=['GET','POST'])
def login():
    if request.method == "POST":
        username = request.form['username']
        password = request.form['password']

        print(username, "successfully logged in.")
        if username == "sc17cf":
            return render_template('home.html')

    return render_template('login.html')

@app.route('/home')
def viewTasks():
    user = "sc17cf"

    return render_template('home.html')

@app.route('/timetable')
def timetable():
    user="sc17cf"

    timetable = timeTableAPI(user)
    return render_template('timetable.html', data = timetable)

@app.route('/coursework')
def coursework():
    user="sc17cf"

    timetable = courseworkAPI(user)
    return render_template('coursework.html', data = timetable)

@app.route('/createNew',  methods=['GET','POST'])
def NewTask():
    return render_template('createNew.html', form = form)
